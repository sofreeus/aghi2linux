---

## Lab 3 Sudo

#### Lab Objectives:

1. Create a file as Tux
2. Create a file as root using sudo

_Create a file as tux_
    Type in the command 'touch tux_file'

_Create a file as root_
    Type in the command 'sudo touch root_file'

_View file ownership
    Type in the command 'll' or 'ls -l'

