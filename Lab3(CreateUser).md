---

## Lab 2 Create your user

#### Lab Objectives:

1. Create a user
2. Set the password for your new user
3. Add sudoer permissions to your new user using the wheel group
4. Reconnect as the new user

#### Create your new user
  
  Enter the following command into your command prompt: 
  
    sudo useradd <username>
  

#### Set the password for your user
  
  Enter the following command into your command prompt, and follow the prompts: 
  
    sudo passwd <username>

#### Add the yourself to the wheel group

  Type in the following command

    sudo usermod -aG wheel <username>


_Reconnect as the new user_

1. Execute the command `exit` or press `[CTRL]+d` to disconnect from the default user.
2. Copy your ssh public key to the new user with `ssh-copy-id`
3. Reconnect to the remote server. `ssh <username>@<IP Address>`


