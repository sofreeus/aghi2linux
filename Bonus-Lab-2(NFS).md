## Lab 4 - Creating an NFS share and mounting other peoples shares.

There is a pretty frequent need to share files across Linux servers and one of the most common ways to do this is to set up a NFS share. In this lab, we are going to create a share on each of our servers and share it with the other students across the background network.


_Creating an NFS SHare_

First we need to install the nfs server on our machines.

```bash
sudo dnf install nfs-utils
```

Next, we'll create the folder that we want to share across machines and set the permissions on the new folder.

```bash
sudo mkdir /srv/nfs
sudo chown nobody:nogroup /srv/nfs
sudo chmod 1777 /srv/nfs
```

Now that the folder is set up, we need to tell the nfs server to who to share that folder with. This is called an Export. To do this, we have to edit the /etc/exports file.

```bash
echo '/class *.sofree.us(rw,sync,no_subtree_check)' | sudo tee /etc/exports
```

Now lets tell NFS to share it.

```bash
sudo systemctl enable nfs-server --now
exportfs -arv
sudo firewall-cmd --permanent --add-service=nfs
firewall-cmd --reload
```

_Mounting your classmates shares_

Now it's time to mount your classmates shares. We're going mount them to the `/mnt` directory by creating a subfolder for each student, making sure we don't accidentally create files in it when it isn't mounted, and then make sure that the mount persists after a reboot.

First lets create the directories for each student and make sure we don't save files to an unmounted directory.

1. Execute `sudo mkdir /mnt/<studentname>` # Repeat for each student.

Now, lets mount the fileshares!

```bash
showmount -e <IP_or_FQDN_of_other_student>
sudo mount <IP_or_FQDN_of_other_student>:/class /mnt/<studentname>
```

Testing Time!!!

Create a file in each of the `/mnt/<studentname>/` directories

```bash
touch /mnt/<studentname>/yourname.txt
# Now you can see the files from each fellow student in your shares!
ls -l /mnt/*/
```

Now lets make them permanent.

1. Execute `sudo nano /etc/fstab`
3. add this for each students IP `<IP_or_FQDN_of_other_student>:/class /mnt/<studentname> nfs defaults 0 0`
3. Test with the commands `sudo umount /mnt/*` and `sudo mount -av`

