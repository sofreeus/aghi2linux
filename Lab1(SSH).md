---

## Lab 1 Connect to your server

#### Lab Objectives:

1. Connect to your server with password authentication
2. Create an SSH Keypair
3. Copy the public key to your server
4. Connect to your server with keypair authent


***Note: This lab does not cover puTTy. If any users are utilizing it, we'll have a small breakout on how to do this.***
_Connect to your server with password authentication
1. In your terminal, type in `ssh tux@<ip address of your assigned machine>`
2. For the password, type in Freedom1234!
3. Once you have successfully logged in, type 'exit' to disconnect

_Create a SSH Keypair and log into your assigned machine_

1. In your terminal, create an SSH Key with the command `ssh-keygen`
2. Select the default location by hitting `[ENTER]`
3. Choose a password for your key and enter it a second time when prompted.
4. This will display a fingerprint and randomart image

_Copy your public key to your assigned server_

1. In your terminal, run the command `ssh-copy-id <username>@<IP Address>`

_Log into your server_

1. It's now time to access your assigned server. Log in with `ssh <username@<IP Address>`
 * note: If you are asked for a password, check your username and/or make sure you copied your ssh key properly.

 ---
 ## Setting up SSH on Windows
 How to install OpenSSH on Windows 10: 

    Open Settings
    Select System
    Select Optional Features
    Check if OpenSSH is already installed
    If not, select Add a feature
    Find OpenSSH Client and select Install
    Find OpenSSH Server and select Install 

How to install OpenSSH on Windows 11: 

    Open Settings
    Select Apps
    Select Optional Features
    Click Add optional features
    Select OpenSSH Server
    Select Install
 
 ## Creating your key with puTTy and copying it onto your server

https://devops.ionos.com/tutorials/use-ssh-keys-with-putty-on-windows/
