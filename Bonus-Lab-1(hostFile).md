## Lab 5 Editing our hosts file

Each of the servers in this class has two IP's. A public IP that allows access from everywhere on the internet and a private IP that only allows connections to the other machines we are using. In this lab, we are going to set up that internal network so that we can communicate between machines. Later, we will set up NFS shares that we can use to share files with.

There are a few steps we need to complete to do this. The first step is to find out what our IP's are.

1. Execute the command `ip a`

This will output information like this:

```bash
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 36:ed:78:c3:69:54 brd ff:ff:ff:ff:ff:ff
    inet 104.131.191.75/20 brd 104.131.191.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet 10.17.0.6/16 brd 10.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::34ed:78ff:fec3:6954/64 scope link
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether e6:89:b5:03:72:e2 brd ff:ff:ff:ff:ff:ff
    inet 10.108.0.3/20 brd 10.108.15.255 scope global eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::e489:b5ff:fe03:72e2/64 scope link
       valid_lft forever preferred_lft forever
```

This is a list of all of your network devices and their connection information. In the example above, lo's IP is 127.0.0.1, eth0 is 104.131.191.75, and eth1 is 10.108.0.3.

1. Take your machine's eth1 address and put it in the Linux room in mattermost in this format `<ipaddress>  <studentname> <studentname>.sofree.us`

Once complete, we're going to create host entries in the /etc/hosts file.

1. Execute the command `sudo nano /etc/hosts`
2. Add each students hostname and IP address into the hosts file. It should look like this:

```bash
...
127.0.0.1 localhost
10.108.0.3 gary.sofree.us gary
10.108.0.4 david.sofree.us david
```

4. Press `[ESC]` twice to exit edit mode.
5. Press `[SHIFT]+Z` twice to save and exit.


You should now be able to `ping` each of the other students machines by their hostnames.

1. `ping <studentname>`
