# Lab 5 Let's make a webserver!

### Find the Apache httpd package
```bash
dnf search httpd
```

### Install the Apache2 httpd server
```bash
sudo dnf install httpd
```
### Make sure that the httpd service is running
```bash
sudo systemctl start httpd
```
---

### Make a simple "Hello, World!" page

Create and edit your web page's index.html file
```bash
sudo nano /var/www/html/index.html
``` 
Sample HTML: 
```html
<h1>Hello world<h1>
```
---
### Let's check our work
Use the following command to check if your page is up
```bash
curl localhost
```
If everything is working, you should see what you just typed into index.html!

Now let's try navigating to your machine in your web browser! Just type your machines IP into the address bar of your favorite browser.

Oh no! We can't access the webpage from our machine!

---
### Enable HTTP so you can see your website
```bash
sudo firewall-cmd --add-service=http --permanent
sudo firewall-cmd --reload
```
Now try going to your webpage!
